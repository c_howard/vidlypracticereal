﻿using System;
using System.Collections.Generic;

namespace Data.Model
{
    public class Video
    {

        public int Id { get; set; }
        public int VideoName { get; set; }
        public DateTime ReleaseDate { get; set; }
        public ICollection<Genre> Genres { get; set; }

    }
}