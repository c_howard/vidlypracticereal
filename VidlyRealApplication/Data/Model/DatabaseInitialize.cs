﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data.Model
{

    public class VidlyContext : DbContext
    {
        public VidlyContext() : base("VidlyContext")
        {
            Database.SetInitializer(new CreateDatabaseIfNotExists<VidlyContext>());

        }
        public DbSet<Video> Videos { get; set; }
        public DbSet<Genre> Genres { get; set; }


    }
    
}
