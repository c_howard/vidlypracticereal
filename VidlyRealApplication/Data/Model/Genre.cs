﻿using System.Collections.Generic;

namespace Data.Model
{
    public class Genre
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public ICollection<Video> Videos { get; set; }

    }
}